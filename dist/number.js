'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var PropTypes = require('prop-types');
var Card = require('creditcards/card');
var Types = require('creditcards/types');

var _require = require('./util'),
    callAll = _require.callAll,
    INPUT_TYPE = _require.INPUT_TYPE;

var AUTOCOMPLETE = 'cardnumber';
var NAME = 'cc-number';
var MASK_CHAR = '•';

module.exports = exports.default = (_temp2 = _class = function (_React$Component) {
  _inherits(NumberPrimitive, _React$Component);

  function NumberPrimitive() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, NumberPrimitive);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = NumberPrimitive.__proto__ || Object.getPrototypeOf(NumberPrimitive)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      value: _this.props.defaultValue || '',
      focused: false
    }, _this.setValue = function () {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      if (value) {
        // parse -> format -> parse to truncate invalid card patterns
        value = Card.parse(Card.format(Card.parse(value)));
      }

      if (_this.isControlled()) {
        _this.props.onChange(_this.getStateAndHelpers({ value: value }));
      } else {
        _this.setState({ value: value }, function () {
          _this.props.onChange(_this.getStateAndHelpers());
        });
      }
    }, _this.handleFocus = function (ev) {
      return _this.setState({ focused: true });
    }, _this.handleBlur = function (ev) {
      return _this.setState({ focused: false });
    }, _this.handleChange = function (ev) {
      return _this.setValue(ev.target.value);
    }, _this.getInputProps = function () {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var value = _this.getValue(props.value);
      return _extends({}, props, {
        'aria-invalid': value ? String(!_this.isValid(value)) : value,
        name: NAME,
        autoComplete: AUTOCOMPLETE,
        type: INPUT_TYPE,
        maxLength: _this.getMaxLength(value),
        placeholder: 'Card number',
        value: _this.props.masked && !_this.state.focused ? _this.props.getMaskedValue(_this.getStateAndHelpers(props)) : Card.format(_this.getValue(props.value)),
        onFocus: callAll(props.onFocus, _this.handleFocus),
        onBlur: callAll(props.onBlur, _this.handleBlur),
        onChange: callAll(props.onChange, _this.handleChange)
      });
    }, _this.getLabelProps = function () {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _extends({}, props, {
        htmlFor: NAME
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(NumberPrimitive, [{
    key: 'isControlled',
    value: function isControlled() {
      return this.props.value !== undefined;
    }
  }, {
    key: 'getValue',
    value: function getValue(value) {
      return value !== undefined ? value : this.isControlled() ? this.props.value : this.state.value;
    }
  }, {
    key: 'isValid',
    value: function isValid() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();

      if (!this.props.cardTypes.length) {
        return Card.isValid(value);
      }
      return this.props.cardTypes.some(function (type) {
        return Card.isValid(value, type);
      });
    }
  }, {
    key: 'getMaxLength',
    value: function getMaxLength(value) {
      var type = Types.get(Card.type(this.getValue(value), true) || '');
      if (!type || !type.digits) return;
      if (Array.isArray(type.digits)) {
        return Math.max.apply(Math, type.digits);
      }
      return type.digits;
    }
  }, {
    key: 'getStateAndHelpers',
    value: function getStateAndHelpers() {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var value = this.getValue(props.value);
      return {
        value: value,
        valid: this.isValid(props.value),
        type: Card.type(value, true),
        setValue: this.setValue,
        getInputProps: this.getInputProps
      };
    }
  }, {
    key: 'render',
    value: function render() {
      return this.props.render(this.getStateAndHelpers());
    }
  }]);

  return NumberPrimitive;
}(React.Component), _class.propTypes = {
  value: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
  render: PropTypes.func.isRequired,
  masked: PropTypes.bool,
  getMaskedValue: PropTypes.func,
  cardTypes: PropTypes.arrayOf(PropTypes.string)
}, _class.defaultProps = {
  cardTypes: [],
  masked: false,
  // By default, mask all but last4.
  getMaskedValue: function getMaskedValue(_ref2) {
    var value = _ref2.value,
        valid = _ref2.valid;

    if (!valid) return Card.format(value);
    return Card.format(value).split(' ').map(function (group, index, array) {
      return index === array.length - 1 ? group : group.replace(/./g, MASK_CHAR);
    }).join(' ');
  },
  onChange: function onChange() {}
}, _temp2);