'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var PropTypes = require('prop-types');
var Cvc = require('creditcards/cvc');
var Types = require('creditcards/types');

var _require = require('./util'),
    callAll = _require.callAll,
    INPUT_TYPE = _require.INPUT_TYPE;

var AUTOCOMPLETE = 'cc-csc';
var NAME = 'cvc';
var MASK_CHAR = '•';

module.exports = exports.default = (_temp2 = _class = function (_React$Component) {
  _inherits(CvcPrimitive, _React$Component);

  function CvcPrimitive() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, CvcPrimitive);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = CvcPrimitive.__proto__ || Object.getPrototypeOf(CvcPrimitive)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      value: _this.props.defaultValue || '',
      focused: false
    }, _this.setValue = function (value) {
      if (!_this.isControlled()) {
        _this.setState({ value: value }, function () {
          _this.props.onChange(_this.getStateAndHelpers());
        });
      } else {
        _this.props.onChange(_this.getStateAndHelpers({ value: value }));
      }
    }, _this.handleFocus = function () {
      return _this.setState({ focused: true });
    }, _this.handleBlur = function () {
      return _this.setState({ focused: false });
    }, _this.handleChange = function (ev) {
      return _this.setValue(ev.target.value);
    }, _this.getInputProps = function () {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _extends({}, props, {
        name: NAME,
        autoComplete: AUTOCOMPLETE,
        type: INPUT_TYPE,
        placeholder: 'CVC',
        maxLength: _this.getMaxLength(),
        pattern: _this.getPattern(),
        value: _this.props.masked && !_this.state.focused ? _this.getValue().replace(/./g, MASK_CHAR) : _this.getValue(),
        onFocus: callAll(props.onFocus, _this.handleFocus),
        onBlur: callAll(props.onBlur, _this.handleBlur),
        onChange: callAll(props.onChange, _this.handleChange)
      });
    }, _this.getLabelProps = function () {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _extends({}, props, {
        htmlFor: NAME
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(CvcPrimitive, [{
    key: 'isControlled',
    value: function isControlled() {
      return this.props.value !== undefined;
    }
  }, {
    key: 'getValue',
    value: function getValue(value) {
      return value !== undefined ? value : this.isControlled() ? this.props.value : this.state.value;
    }
  }, {
    key: 'isValid',
    value: function isValid() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();

      return Cvc.isValid(value, this.props.cardType);
    }
  }, {
    key: 'getMaxLength',
    value: function getMaxLength() {
      var restrictedType = Types.get(this.props.cardType || '');
      return restrictedType !== undefined ? restrictedType.cvcLength : 4;
    }
  }, {
    key: 'getPattern',
    value: function getPattern() {
      var restrictedType = Types.get(this.props.cardType || '');
      return restrictedType !== undefined ? '[0-9]{' + restrictedType.cvcLength + '}' : '[0-9]{3,4}';
    }
  }, {
    key: 'getStateAndHelpers',
    value: function getStateAndHelpers() {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      return {
        focused: this.state.focused,
        value: this.getValue(props.value),
        valid: this.isValid(props.value),
        setValue: this.setValue,
        getInputProps: this.getInputProps
      };
    }
  }, {
    key: 'render',
    value: function render() {
      return this.props.render(this.getStateAndHelpers());
    }
  }]);

  return CvcPrimitive;
}(React.Component), _class.propTypes = {
  value: PropTypes.string,
  masked: PropTypes.bool,
  cardType: PropTypes.string,
  onChange: PropTypes.func,
  defaultValue: PropTypes.string,
  render: PropTypes.func.isRequired
}, _class.defaultProps = {
  masked: false,
  onChange: function onChange() {}
}, _temp2);