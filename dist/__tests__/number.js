'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var React = require('react');

var _require = require('enzyme'),
    mount = _require.mount;

var Number = require('../number');

function setup() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var _ref$render = _ref.render,
      render = _ref$render === undefined ? function () {
    return React.createElement('div', null);
  } : _ref$render,
      props = _objectWithoutProperties(_ref, ['render']);

  var renderArg = void 0;
  var renderSpy = jest.fn(function (arg) {
    renderArg = arg;
    return render(arg);
  });
  var wrapper = mount(React.createElement(Number, _extends({}, props, { render: renderSpy })));
  return _extends({ renderSpy: renderSpy, wrapper: wrapper }, renderArg);
}

test('value formatting', function () {
  var _setup = setup(),
      wrapper = _setup.wrapper,
      setValue = _setup.setValue,
      getInputProps = _setup.getInputProps;

  setValue('424242');
  expect(getInputProps().value).toEqual('4242 42');

  setValue('4242424242424242');
  expect(getInputProps().value).toEqual('4242 4242 4242 4242');
});

test('validation on all card types', function () {
  var _setup2 = setup(),
      wrapper = _setup2.wrapper,
      valid = _setup2.valid,
      setValue = _setup2.setValue,
      getInputProps = _setup2.getInputProps;

  expect(getInputProps()).toMatchSnapshot('basic no input');
  expect(valid).toBe(false);
  setValue('378282246310005');
  expect(getInputProps()).toMatchSnapshot('basic amex valid');
  expect(wrapper.instance().isValid()).toBe(true);
  setValue('37828224631000');
  expect(getInputProps()).toMatchSnapshot('basic amex invalid');
  expect(wrapper.instance().isValid()).toBe(false);
});

test('validation on given card types', function () {
  var _setup3 = setup({
    cardTypes: ['Visa']
  }),
      wrapper = _setup3.wrapper,
      valid = _setup3.valid,
      setValue = _setup3.setValue;

  expect(valid).toBe(false);
  setValue('378282246310005');
  expect(wrapper.instance().isValid()).toBe(false);
  expect(wrapper.instance().getStateAndHelpers()).toMatchSnapshot();

  // Make it Visa and it will be valid.
  setValue('4242424242424242');
  expect(wrapper.instance().isValid()).toBe(true);
  expect(wrapper.instance().getStateAndHelpers()).toMatchSnapshot();
});

test('masked', function () {
  var _setup4 = setup({
    masked: true
  }),
      setValue = _setup4.setValue,
      getInputProps = _setup4.getInputProps;

  // Invalid card (missing one digit)


  setValue('424242424242424');
  expect(getInputProps().value).toEqual('4242 4242 4242 424');

  // Valid AMEX
  setValue('378282246310005');
  expect(getInputProps().value).toEqual('•••• •••••• 10005');
});

test('onChange uncontrolled', function () {
  var changeData = void 0;

  var _setup5 = setup({
    onChange: function onChange(data) {
      changeData = data;
    }
  }),
      setValue = _setup5.setValue,
      wrapper = _setup5.wrapper;

  expect(changeData).toBe(undefined);
  setValue('42424');
  expect(changeData.value).toBe('42424');
  expect(changeData.valid).toBe(false);
  expect(changeData.type).toBe('Visa');

  expect(wrapper.state('value')).toEqual('42424');
});

test('onChange w/ controlled input', function () {
  var changeData = void 0;

  var _setup6 = setup({
    value: '',
    onChange: function onChange(data) {
      changeData = data;
    }
  }),
      wrapper = _setup6.wrapper,
      setValue = _setup6.setValue;

  expect(changeData).toBe(undefined);
  setValue('42424');
  expect(changeData.value).toBe('42424');
  expect(changeData.valid).toBe(false);
  expect(changeData.type).toBe('Visa');

  // No setState because it's a controlled input
  expect(wrapper.state('value')).toEqual('');
});

test('defaultValue', function () {
  var _setup7 = setup({
    defaultValue: '4242'
  }),
      wrapper = _setup7.wrapper,
      setValue = _setup7.setValue;

  expect(wrapper.state('value')).toBe('4242');
  setValue('');
  expect(wrapper.state('value')).toBe('');
});