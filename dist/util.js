'use strict';

var isAndroid = require('is-android');

exports.INPUT_TYPE = isAndroid ? 'tel' : 'text';

exports.callAll = function () {
  for (var _len = arguments.length, fns = Array(_len), _key = 0; _key < _len; _key++) {
    fns[_key] = arguments[_key];
  }

  return function (arg) {
    return fns.forEach(function (fn) {
      return fn && fn(arg);
    });
  };
};